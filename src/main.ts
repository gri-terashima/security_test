import { NestFactory } from '@nestjs/core';
import { AppModule } from './api/app.module';
import { join } from 'path';
import * as express from 'express';
import { db } from './db/db';


async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.setGlobalPrefix('/api');

    // DBの初期化
    db().init();

    await app.listen(3000);
}
bootstrap();
