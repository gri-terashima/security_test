import { Injectable } from '@nestjs/common';
import { db } from 'src/db/db';

@Injectable()
export class SqlInjectionService {

    async getMessage(id: number = null) {
        try {
            // 会社ID1の情報を検索する
            let query = 'select * from test_sql_injection where company_id = 1';

            // IDが指定されている場合は条件にIDを付与
            if (id) {
                query += ' and id = ' + id;
            }

            const result = await db().query(query);
            if (result && result.rows) {
                // 取得結果を返却
                return result.rows
            } else {
                return null;
            }
        } catch (err) {
            throw err;
        }
    }

    async addMessage(message: string) {
        try {
            // 会社IDは1固定にしておく
            const query = 'insert into test_sql_injection (company_id, message) values (1,\'' + message + '\')';

            const result = await db().query(query);
            return result;
        } catch (err) {
            throw err;
        }
    }
}
