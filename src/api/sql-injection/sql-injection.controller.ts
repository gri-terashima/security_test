import { Controller, Get, Post, Body, Query } from '@nestjs/common';
import { SqlInjectionService } from './sql-injection.service';

@Controller({ path: 'sql-injection' })
export class SqlInjectionController {
    constructor(private readonly service: SqlInjectionService) { }

    @Get()
    getData(@Query('id') id: number): any {
        //if (!id) throw new Error('id is required.');

        return this.service.getMessage(id);
    }

    @Post()
    postData(@Body() body: {message: string}): any {
        return this.service.addMessage(body.message);
    }
}
