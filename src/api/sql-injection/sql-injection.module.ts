import { Module } from '@nestjs/common';
import { SqlInjectionController } from './sql-injection.controller';
import { SqlInjectionService } from './sql-injection.service';

@Module({
  imports: [],
  controllers: [SqlInjectionController],
  providers: [SqlInjectionService],
})
export class SqlInjectionModule {}
