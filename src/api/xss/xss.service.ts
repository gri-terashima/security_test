import { Injectable } from '@nestjs/common';
import { db } from 'src/db/db';

@Injectable()
export class XssService {
    async getMessages() {
        try {
            const result = await db().query('select * from test');
            return result.rows;
        } catch (err) {
            throw err;
        }
    }

    async addMessage(msg: string) {
        try {
            return await db().query('insert into test (message) values (\'' + msg + '\')');
        } catch (err) {
            throw err;
        }
    }
}
