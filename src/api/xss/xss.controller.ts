import { Controller, Get, Post, Body } from '@nestjs/common';
import { XssService } from './xss.service';

@Controller({ path: 'xss' })
export class XssController {

    constructor(private readonly service: XssService) { }

    @Get()
    getData() {
        return this.service.getMessages();
    }

    @Post()
    postData(@Body() body: { message: string }) {
        return this.service.addMessage(body.message);
    }
}
