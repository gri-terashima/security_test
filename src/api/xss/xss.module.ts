import { Module } from '@nestjs/common';
import { XssController } from './xss.controller';
import { XssService } from './xss.service';

@Module({
  imports: [],
  controllers: [XssController],
  providers: [XssService],
})
export class XssModule {}
