import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { XssModule } from './xss/xss.module';
import { SqlInjectionModule } from './sql-injection/sql-injection.module';

@Module({
    imports: [
        ServeStaticModule.forRoot({
            // htmlファイルへのルーティング
            rootPath: join(__dirname, '../..', 'public'),
        }),

        XssModule,
        SqlInjectionModule,
    ],
})
export class AppModule { }
