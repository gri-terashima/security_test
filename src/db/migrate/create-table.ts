export const CREATE_TABLE_TEST = `create table if not exists test (
    id serial not null PRIMARY KEY,
    message text
)`;

export const CREATE_TABLE_TEST_SQL_INJECTION = `create table if not exists test_sql_injection (
    id serial not null PRIMARY KEY,
    company_id int not null,
    message text
)`;

export const CREATE_TABLES = [
    CREATE_TABLE_TEST,
    CREATE_TABLE_TEST_SQL_INJECTION,
];
