export const INSERT_TEST = [
    'insert into "public".test(id,message) values (1,\'www\');',
    'insert into "public".test(id,message) values (2,\'ワロタ\');',
    'insert into "public".test(id,message) values (3,\'大草原\');',
    'insert into "public".test(id,message) values (4,\'おい！このサイトすげぇぞ！！！<a href="#" onclick=alert("cookieゲット～\\n"+document.cookie)>http://www.google.com</a>\');',
    // 参照p-keyのずれを修正
    'SELECT setval(\'test_id_seq\', (SELECT MAX(id) FROM test));',
];

export const INSERT_SQL_INJECTION = [
    'insert into "public".test_sql_injection(id,company_id,message) values (0,99,\'見えちゃダメなやつ\');             ',
    'insert into "public".test_sql_injection(id,company_id,message) values (1,1,\'test\');                            ',
    'insert into "public".test_sql_injection(id,company_id,message) values (2,1,\'test2\');                           ',
    'insert into "public".test_sql_injection(id,company_id,message) values (3,1,\'test3\');                           ',
    'insert into "public".test_sql_injection(id,company_id,message) values (4,1,\'test4\');                           ',
    'insert into "public".test_sql_injection(id,company_id,message) values (5,1,\'test5\');                           ',
    'insert into "public".test_sql_injection(id,company_id,message) values (6,1,\'test6\');                           ',
    'insert into "public".test_sql_injection(id,company_id,message) values (7,1,\'test7\');                           ',
    'insert into "public".test_sql_injection(id,company_id,message) values (8,1,\'test8\');                           ',
    'insert into "public".test_sql_injection(id,company_id,message) values (9,1,\'test9\');                           ',
    'insert into "public".test_sql_injection(id,company_id,message) values (10,1,\'test10\');                         ',
    'insert into "public".test_sql_injection(id,company_id,message) values (11,2,\'会社ID2のデータ　※見えちゃダメ\');',
    'insert into "public".test_sql_injection(id,company_id,message) values (12,3,\'会社ID3のデータ　※見えちゃダメ\');',
    'insert into "public".test_sql_injection(id,company_id,message) values (13,4,\'会社ID4のデータ　※見えちゃダメ\');',
    'insert into "public".test_sql_injection(id,company_id,message) values (14,5,\'会社ID5のデータ　※見えちゃダメ\');',
    // 参照p-keyのずれを修正
    'SELECT setval(\'test_sql_injection_id_seq\', (SELECT MAX(id) FROM test_sql_injection));',
];

export const INSERTS = [
    ...INSERT_TEST,
    ...INSERT_SQL_INJECTION,
]