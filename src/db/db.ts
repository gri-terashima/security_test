import { Client } from 'pg';
import { CREATE_TABLES } from './migrate/create-table';
import { INSERTS } from './migrate/insert';

let inst;

export const db = () => {
    if (!inst) {
        inst = new DB();
    }
    return inst;
}

class DB {
    readonly config = {
        user: process.env.POSTGRES_USER,
        host: process.env.POSTGRES_HOST,
        database: process.env.POSTGRES_DB_NAME,
        password: process.env.POSTGRES_PASSWORD,
        port: process.env.POSTGRES_PORT,
    };
    private db;

    async init() {
        try {
            const db = new Client(this.config);
            await db.connect();
    
            this.db = db;

            await this.migration();
        } catch (err) {
            console.log(err);
        }
    }

    private async migration() {
        await this.createTables();
        this.insertRecords();
    }

    private async createTables() {
        const promises = [];
        for (const query of CREATE_TABLES) {
            promises.push(this.db.query(query));
        }
        return Promise.all(promises);
    }

    private async insertRecords() {
        for (const query of INSERTS) {
            try{
                this.db.query(query);
            } catch(err) {
                // P-keyかぶりで2回目以降は絶対にエラーになるため無視
            }    
        }
    }

    async query(sql: string, values = []) {
        try {
            console.log(sql);
            console.log('values:', values);
            return await this.db.query(sql, values);
        } catch(err) {
            throw err;
        }
    }
}