class Http {
    constructor() {
        this.basePath = '/api';
    }

    get(url, query = null) {
        const req = new XMLHttpRequest();
        req.open('GET', this.basePath + url + (query ? '?' + query : ''), true);
        req.send(null);

        return new Promise((resolve, reject) => {
            req.onreadystatechange = () => {
                if (req.readyState == 4 && req.status == 200) {
                    console.log('get complete', req)
                    resolve(JSON.parse(req.responseText));
                }
            };
        });
    }

    post(url, body) {
        const req = new XMLHttpRequest();
        req.open('POST', this.basePath + url, true);
        req.setRequestHeader('content-type', 'application/json;charset=UTF-8');
        req.send(JSON.stringify(body));

        return new Promise((resolve, reject) => {
            req.onreadystatechange = () => {
                if (req.readyState == 4 && req.status == 201) {
                    console.log('post complete', req)
                    resolve(req.responseText);
                }
            };
        });
    }
}

const http = new Http();