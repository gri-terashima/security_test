const getQueryParams = function () {
    // 先頭の?を削除
    const query = window.location.search.slice(1);
    // パラメータ毎に分割して配列化
    const queryParams = query.split('&');

    // オブジェクト形式に変換
    const params = {};
    queryParams.forEach(q => {
        const split = q.split('=');
        params[split[0]] = split[1];
    });

    return params;
}
